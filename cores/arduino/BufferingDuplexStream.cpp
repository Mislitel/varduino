#include <limits>

#include "BufferingDuplexStream.h"

namespace arduino
{

size_t BufferingDuplexStream::write(uint8_t nChar)
{
    return m_TxBuffer.write(nChar)
           ? 1
           : 0;
}

int BufferingDuplexStream::available()
{
    return m_RxBuffer.available();
}

int BufferingDuplexStream::peek()
{
    return m_RxBuffer.peek();
}

int BufferingDuplexStream::read()
{
    return m_RxBuffer.read();
}

} /* namespace arduino */
