#include <iostream>

#include "HardwareSerial.h"

namespace arduino
{

void HardwareSerial::begin(unsigned long)
{}
void HardwareSerial::begin(unsigned long baudrate, uint16_t config)
{}

void HardwareSerial::end()
{}
int HardwareSerial::available(void)
{
    return 0;
}
int HardwareSerial::peek(void)
{
    return 0;
}
int HardwareSerial::read(void)
{
    return 0;
}
void HardwareSerial::flush(void)
{
    std::cout.flush();
}
size_t HardwareSerial::write(uint8_t value)
{
    std::cout << value;
    std::cout.flush();
    return 1;
}

HardwareSerial::operator bool()
{
    return true;
}

} /* namespace arduino */

arduino::HardwareSerial Serial;
