#pragma once

#include <mutex>
#include <map>

#include "Types.h"
#include "ecp/ECP_Types.h"

#include "BufferingDuplexStream.h"

namespace arduino
{

namespace ecp
{

std::map<En_LineType, DuplexStreamConnector>& ECP_Connectors();

} /* namespace ecp */

} /* namespace arduino */
