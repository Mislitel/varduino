#pragma once

namespace arduino
{
namespace ecp
{

// Константы взяты из документации Лёши Егошина по протоколу ECP:
// https://gitlab.com/itmo-iot-lab-group/virtual_lab/-/blob/server_refactoring/docs/UNIX%20SOCKET%20API%20doc.md
// 10.12.2021
enum class En_LineType
{
    WI_FI = 1,
    I2C = 2,
};


} /* namespace ecp */
} /* namespace arduino */
