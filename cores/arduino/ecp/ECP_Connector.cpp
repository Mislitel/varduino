#include "ecp/ECP_Connector.h"

namespace arduino
{

namespace ecp
{

static std::map<En_LineType, DuplexStreamConnector> mapECP_Connectors;

std::map<En_LineType, DuplexStreamConnector>& ECP_Connectors()
{
    return mapECP_Connectors;
}

} /* namespace ecp */

} /* namespace arduino */
