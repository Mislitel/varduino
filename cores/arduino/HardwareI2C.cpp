#include "HardwareI2C.h"

namespace arduino
{

void HardwareI2C::begin()
{}
void HardwareI2C::begin(uint8_t address)
{}
void HardwareI2C::end()
{}

void HardwareI2C::setClock(uint32_t freq)
{}

void HardwareI2C::beginTransmission(uint8_t address)
{}
uint8_t HardwareI2C::endTransmission(bool stopBit)
{
    return 0;
}
uint8_t HardwareI2C::endTransmission(void)
{
    return 0;
}

size_t HardwareI2C::requestFrom(uint8_t address, size_t len, bool stopBit)
{
    return 0;
}
size_t HardwareI2C::requestFrom(uint8_t address, size_t len)
{
    return 0;
}

void HardwareI2C::onReceive(void(*)(int))
{}
void HardwareI2C::onRequest(void(*)(void))
{}

HardwareI2C HwI2C = HardwareI2C();

} /* namespace arduino */
