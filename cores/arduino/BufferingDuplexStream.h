#pragma once

#include <optional>
#include <mutex>
#include <vector>
#include <limits>

#include "Types.h"

#include "RingBuffer.h"
#include "Stream.h"

namespace arduino {

constexpr size_t RX_RING_BUFFER_SIZE_BYTES = 128;  // с потолка
constexpr size_t TX_RING_BUFFER_SIZE_BYTES = 128;  // с потолка

namespace __detail
{

template <size_t N>
class ThreadSafeRingBuffer
{
    using TMutex = std::mutex;

public:
    bool write(uint8_t Char)
    {
        std::lock_guard<TMutex> Lock(m_Mutex);
        if (m_RingBuffer.isFull())
        {
            return false;
        }
        m_RingBuffer.store_char(Char);
        return true;
    }
    int read()
    {
        std::lock_guard<TMutex> Lock(m_Mutex);
        return m_RingBuffer.read_char();
    }
    int available()
    {
        std::lock_guard<TMutex> Lock(m_Mutex);
        return m_RingBuffer.available();
    }
    int peek()
    {
        std::lock_guard<TMutex> Lock(m_Mutex);
        return m_RingBuffer.peek();
    }

private:
    TMutex m_Mutex;
    RingBufferN<N> m_RingBuffer;
};

} /* namespace detail */

using TRxBuffer = __detail::ThreadSafeRingBuffer<RX_RING_BUFFER_SIZE_BYTES>;
using TTxBuffer = __detail::ThreadSafeRingBuffer<TX_RING_BUFFER_SIZE_BYTES>;

class DuplexStreamConnector
{

public:
    DuplexStreamConnector(TTxBuffer& Tx, TRxBuffer& Rx)
    : m_TxBuffer(Tx)
    , m_RxBuffer(Rx)
    {}

    void write(const ByteVector& vData)
    {
        for (const auto Byte: vData)
        {
            m_RxBuffer.write(Byte);    // не ошибка, transmit для отправляющей стороны, т. е. поступает в наш буфер на приём
        }
    }
    ByteVector read()
    {
        ByteVector vResult = {};
        while (m_TxBuffer.available())
        {
            uint32_t Char = m_TxBuffer.read();    // не ошибка, receive для отправляющей стороны, т. е. данные берутся из нашего буфер на отправку
            if (Char > std::numeric_limits<uint8_t>::max())
            {
                throw std::runtime_error("DuplexChannel: invalid char size");
            }
            vResult.emplace_back(static_cast<uint8_t>(m_TxBuffer.read()));
        }
        return vResult;
    }

private:
    TTxBuffer& m_TxBuffer;
    TRxBuffer& m_RxBuffer;
};



class BufferingDuplexStream: public Stream
{

public:
    virtual ~BufferingDuplexStream() {}

    virtual size_t write(uint8_t) override;
    virtual int available() override;
    virtual int read() override;
    virtual int peek() override;

    DuplexStreamConnector Connector() { return DuplexStreamConnector(m_TxBuffer, m_RxBuffer); }

private:
    TTxBuffer m_TxBuffer = {};
    TRxBuffer m_RxBuffer = {};
};

} /* namespace arduino */
