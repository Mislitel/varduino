#include <chrono>
#include <thread>

#include <iostream>

#include "Arduino.h"
#include "HardwareI2C.h"

#include "ecp/ECP_Connector.h"

#define VARDUINO_VERSION "v0.0.5-dummy"
#define VARDUINO_PRODUCT_INFO "Varduino: Virtual Arduino Board " VARDUINO_VERSION

static void app_init()
{
    arduino::ecp::ECP_Connectors().emplace(
        arduino::ecp::En_LineType::I2C,
        arduino::HwI2C.Connector()
    );
}

int main()
{
    try
    {
        app_init();

        std::cout << VARDUINO_PRODUCT_INFO << std::endl;
        setup();
        for(;;)
        {
           loop();
           std::this_thread::sleep_for(std::chrono::milliseconds(5));
        }
    }
    catch (std::exception& e)
    {
        std::cerr << "Terminated by exception:" << e.what() << std::endl;
        return -1;
    }
}
