#pragma once

#include <vector>

namespace arduino
{

using ByteVector = std::vector<uint8_t>;

} /* namespace arduino */
