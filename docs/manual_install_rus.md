## Ручная установка (на *NIX)

1. Поставить утилиту `arduino-cli`.
2. Посмотреть, в какой каталог утилита `arduino-cli` сохраняет свои core:

``` bash
$ arduino-cli config dump

...
directories:
  data: /Users/oops/Library/Arduino15
  downloads: /Users/oops/Library/Arduino15/staging
  user: /Users/oops/Documents/Arduino    # <-- вот он
...
```
3. Перейти в этот каталог:
``` bash
$ cd <dir>
```
4. Создать в нём структуру подкаталогов и склонировать туда этот репозиторий:
```bash
$ mkdir -p hardware/iotlab
$ cd hardware/iotlab
$ git clone https://gitlab.com/Mislitel/varduino x86_64
```
5. Проверить, что в списке core у `arduino-cli` появился новый core:
``` bash
$ arduino-cli core list
...
ID            Installed Latest Name
arduino:avr   1.8.3     1.8.3  Arduino AVR Boards
...
iotlab:x86_64    # <-- вот он
```

6. Проверить работоспособность компилятора. В каталоге `ino` есть скетч `Dummy`, который который можно собрать для тестирования. Структура каталогов скетчей должна выглядеть так:
```
Dummy
|_ Dummy.ino
|_ другие файлы C/C++ при наличии
```
7. Перейти в директорию и собрать скетч:
``` bash
$ cd Dummy
$ arduino-cli compile -b iotlab:x86_64:varduino -e .
```

*Примечание:* флаг `-e` требуется, чтобы собранный артефакт появился в рабочем каталоге в `build`. Без него артефакты останутся где-то среди временных файлов. Более старые версии `arduino-cli` экспортировали артефакты в рабочий каталог и без флага `-e`.

8. Запустить:
``` bash
$ cd build/iotlab.x86_64.varduino
$ ./Dummy.ino.elf

Varduino: Virtual Arduino Board v.X.X.X
# <- и здесь мы крутимся в бесконечном цикле, пока не прибьём процесс :) ->
```
